using UnityEditor;
using UnityEngine;
 
[CustomEditor(typeof(EndLevelTrigger)), CanEditMultipleObjects]
public class TextAreaEditor : Editor {
     
    public SerializedProperty endText;
    void OnEnable () {
        endText = serializedObject.FindProperty ("endText");
    }
     
    public override void OnInspectorGUI() {
        serializedObject.Update ();
        endText.stringValue = EditorGUILayout.TextArea( endText.stringValue, GUILayout.MaxHeight(275) );
        serializedObject.ApplyModifiedProperties ();
    }
}