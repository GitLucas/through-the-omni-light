using UnityEngine;

public class ObjectPreview : Interactable
{
    [Header("Objects")] 
    [SerializeField] private Collider2D myCollider;
    [SerializeField] private TextTypewriter messageBox;

    [Header("Properties")] 
    [SerializeField] private string message;
    [SerializeField] private GameObject preview;
    
    
    public override void Interact()
    {
        base.Interact();
            
        myCollider.enabled = false;
        
        messageBox.gameObject.SetActive(true);
        preview.SetActive(true);

        messageBox.OnTypewriteFinished += Otf;
        messageBox.Write(message);
    }

    private void Otf()
    {
        preview.SetActive(false);
        InvokeInteraction();
        //gameObject.SetActive(false);
        messageBox.OnTypewriteFinished -= Otf;
    }
}
