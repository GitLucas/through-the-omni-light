using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    [SerializeField] private GameObject highlight;
    [SerializeField] private UnityEvent interactionEvent;

    public void HighlightObject(bool cond) => highlight.SetActive(cond);
    
    public static event Action OnInteraction;
    
    public virtual void Interact()
    {
        interactionEvent?.Invoke();
    }

    protected void InvokeInteraction()
    {
        OnInteraction?.Invoke();
    }
}
