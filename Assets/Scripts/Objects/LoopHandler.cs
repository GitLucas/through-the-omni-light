using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopHandler : MonoBehaviour
{
    [SerializeField] private CaveUtils utils;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;
        
        utils.OnLoop();
    }
}
