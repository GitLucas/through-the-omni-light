using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxController : MonoBehaviour
{
    
    #region Singleton

    private static SfxController _instance;
    public static SfxController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<SfxController>();

            return _instance;
        }
    } 
    
    #endregion

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }
        
        DontDestroyOnLoad(gameObject);

        _instance = this;
    }

    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip
        casette, chair, crow, fieldVoices, tvOn, casetteVoices, footstep;

    public void PlayCasette() => source.PlayOneShot(casette);
    public void PlayChair() => source.PlayOneShot(chair);
    public void PlayCrow() => source.PlayOneShot(crow);
    public void PlayFieldVoices() => source.PlayOneShot(fieldVoices);
    public void PlayTvOn() => source.PlayOneShot(tvOn);
    public void PlayCasetteVoices() => source.PlayOneShot(casetteVoices);

    public void PlayFootstep() => source.PlayOneShot(footstep);


}
