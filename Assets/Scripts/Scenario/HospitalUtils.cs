using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalUtils : MonoBehaviour
{
    [SerializeField] private EndLevelTrigger ender;

    public void EndLevel() => StartCoroutine(Ending());

    private IEnumerator Ending()
    {
        yield return new WaitForSeconds(2f);
        ender.EndLevel();
    }

    public void PlayTVSound() => SfxController.Instance.PlayTvOn();
    public void PlayCasette() => SfxController.Instance.PlayCasette();
    public void PlayCasetteVoices() => SfxController.Instance.PlayCasetteVoices();
}
