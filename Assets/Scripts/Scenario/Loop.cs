using System;
using UnityEngine;
using UnityEngine.Events;

public class Loop : MonoBehaviour
{
    [SerializeField] private GameObject[] loop;
    [SerializeField] private float maxDistance, gap;
    
    public bool HaveToLoop { get; set; }

    private Transform _cam;
    private int _index;

    private void Start()
    {
        HaveToLoop = true;
        if (Camera.main != null) _cam = Camera.main.transform;
    }

    private void Update()
    {
        if (!HaveToLoop) return;
        
        if (ShouldSpawnNext())
            SpawnNext();
    }

    private void SpawnNext()
    {
        var cameraX = _cam.position.x;
        var moduleX = loop[_index].transform.position.x;
        var prev = _index;

        int direction;
        
        if (cameraX > moduleX)
        {
            _index = (loop.Length + _index - 1) % loop.Length;
            direction = 1;
        }
        else
        {
            _index = (_index + 1) % loop.Length;
            direction = -1;
        }
        
        loop[_index].transform.position = loop[prev].transform.position + Vector3.right * (gap * direction);
    }

    private bool ShouldSpawnNext()
    {
        return Mathf.Abs(_cam.position.x - loop[_index].transform.position.x) > maxDistance;
    }
}
