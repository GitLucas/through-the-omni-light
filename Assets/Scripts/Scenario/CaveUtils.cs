using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveUtils : HospitalUtils
{
    [SerializeField] private Interactable loops;
    private int _loops;
    private bool _ended;
    
    public void OnLoop()
    {
        if (_ended || ++_loops < 5) return;
        
        _ended = true;
        loops.Interact();
    }
}
