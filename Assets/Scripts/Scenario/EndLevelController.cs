using System;
using UnityEngine;
using UnityEngine.Events;

public class EndLevelController : MonoBehaviour
{
    [SerializeField] private UnityEvent onMilestoneReached;
    [SerializeField] private int milestone = 3;
    
    private int _interactions;
    
    private void Start()
    {
        Interactable.OnInteraction += HandlePlayerUpgrade;
    }

    public void HandlePlayerUpgrade()
    {
        Debug.Log(_interactions);
        if(++_interactions < milestone) return;

        onMilestoneReached?.Invoke();
    }
}
