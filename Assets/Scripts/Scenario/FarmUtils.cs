using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmUtils : MonoBehaviour
{
    [SerializeField] private Transform leftPivot, rightPivot;
    [SerializeField] private CameraController cameraController;

    [Space] 
    [SerializeField] private Transform finalPart;
    [SerializeField] private Transform finalTransform;

    public void StablishBoundaries()
    {
        finalPart.position = finalTransform.position;
        
        cameraController.SetLimits(leftPivot.position.x - 1.75f, rightPivot.position.x + 5f);
    }
    
    public void PlayTVSound() => SfxController.Instance.PlayTvOn();
    public void PlayCrow() => SfxController.Instance.PlayCrow();
    public void PlayChair() => SfxController.Instance.PlayChair();
    public void PlayVoices() => SfxController.Instance.PlayFieldVoices();
}
