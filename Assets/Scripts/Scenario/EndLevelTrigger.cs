using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndLevelTrigger : MonoBehaviour
{
    [SerializeField] private GameObject whiteScreenGo;
    [SerializeField] private Image whiteScreen;
    [SerializeField] private TextTypewriter writer;
    [SerializeField] private string endText;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;

        EndLevel();
    }

    public void EndLevel()
    {
        PlayerStatusController.Instance.SetInactive();
        
        whiteScreenGo.SetActive(true);
        StartCoroutine(Ending());
    }

    private IEnumerator Ending()
    {
        for (float i = 0; i < 1; i += .0625f)
        {
            var whiteScreenColor = whiteScreen.color;
            whiteScreenColor.a = i;
            whiteScreen.color = whiteScreenColor;
            yield return new WaitForEndOfFrame();

        }

        whiteScreen.color = Color.white;
        
        writer.Write(endText);
        writer.OnTypewriteFinished += () =>
        {
            StartCoroutine(WaitAndMove());
        };
    }

    private IEnumerator WaitAndMove()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % 4);
    }
}
