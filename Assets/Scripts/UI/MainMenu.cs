using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    [SerializeField] protected Image playCrossing, fullscreenCrossing;
    [SerializeField] private Button playButton;

    public void Play() => StartCoroutine(PlayCoroutine());

    private IEnumerator PlayCoroutine()
    {
        playButton.interactable = false;

        yield return Cross(playCrossing);
        
        SceneManager.LoadScene(1);
    }

    protected IEnumerator Cross(Image image)
    {
        for (var i = 0f; i < 1; i += .0625f)
        {
            image.fillAmount = i;
            yield return new WaitForEndOfFrame();
        }

        fullscreenCrossing.fillAmount = 1;
    }
    
    protected IEnumerator Uncross(Image image)
    {
        for (var i = 1f; i > 0; i -= .0625f)
        {
            image.fillAmount = i;
            yield return new WaitForEndOfFrame();
        }

        fullscreenCrossing.fillAmount = 0;
    }

    public void ToggleFullscreen()
    {
        Screen.fullScreen = !Screen.fullScreen;

        Debug.Log(Screen.fullScreen);

        StartCoroutine(Screen.fullScreen ? Cross(fullscreenCrossing) : Uncross(fullscreenCrossing));
    }
    
    private void OnEnable()
    {
        fullscreenCrossing.fillAmount = Screen.fullScreen ? 0 : 1;
    }
}
