using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameCanvas : MonoBehaviour
{
    [SerializeField] private GameObject pauseCanvas;
    
    
    private void Update()
    {
        if (!Input.GetButtonDown("Pause")) return;
        
        pauseCanvas.SetActive(!pauseCanvas.activeInHierarchy);

        if (pauseCanvas.activeInHierarchy)
        {
            PlayerStatusController.Instance.SetInactive();
        }
        else
        {
            PlayerStatusController.Instance.SetNormal();
        }
        
    }
}
