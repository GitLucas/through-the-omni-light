using UnityEngine;
using UnityEngine.EventSystems;

public class MenuCover : MonoBehaviour, IPointerClickHandler, IPointerDownHandler
{
    private GameObject _current;
    private void Update()
    {
        _current = EventSystem.current.currentSelectedGameObject;
    }

    public void OnPointerDown(PointerEventData data)
    {
        EventSystem.current.SetSelectedGameObject(_current);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        
    }
}
