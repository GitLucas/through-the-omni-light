using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MainMenu
{
    [Header("Pause")] [Space]
    [SerializeField] private Image quitCrossing;
    [SerializeField] private Button quit;

    private void Awake()
    {
        fullscreenCrossing.fillAmount = Screen.fullScreen ? 0 : 1;
        gameObject.SetActive(false);
    }

    public void Quit()
    {
        StartCoroutine(Exit());
    }
    
    private IEnumerator Exit()
    {
        quit.interactable = false;

        yield return Cross(quitCrossing);
        
        SceneManager.LoadScene(0);
    }
}
