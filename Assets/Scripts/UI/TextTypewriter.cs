using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextTypewriter : MonoBehaviour
{
    public bool Active
    {
        get;
        set;
    }
    
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private bool destroyAfterFinished = true;

    public event Action OnTypewriteFinished;
    
    private Coroutine _typewrite;
    private string _msg;

    private void Start()
    {
        text.text = "";
    }

    private void Update()
    {
        if (!Active) return;
        
        if (!Input.GetButtonDown("Vertical")) return;

        if (_typewrite == null) return;
        
        StopCoroutine(_typewrite);
        _typewrite = null;
        StartCoroutine(EndTypewriting());
    }

    public void Write(string msg)
    {
        _msg = msg;
        Active = true;
        _typewrite = StartCoroutine(TypeWrite());
    }

    private IEnumerator TypeWrite()
    {
        
        foreach (var t in _msg)
        {
            yield return new WaitForSeconds(.0625f);
            text.text += t;
        }

        yield return EndTypewriting();
    }

    private IEnumerator EndTypewriting()
    {
        text.text = _msg;
        Active = false;

        yield return new WaitForSeconds(1.5f);
        if (destroyAfterFinished)
        {
            gameObject.SetActive(false);
            PlayerStatusController.Instance.SetNormal();
            PlayerStatusController.Instance.SetAnimatorSpeed(1);
        }
        OnTypewriteFinished?.Invoke();
        
        text.text = "";
    }
}
