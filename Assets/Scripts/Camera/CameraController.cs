using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private PlayerMovement playerMovement;
    [SerializeField] private Transform player;

    [SerializeField] private float diff;

    [Space] 
    [SerializeField] private bool checkBoundaries;
    [SerializeField] private float minX, maxX;

    public bool CheckBoundaries
    {
        get => checkBoundaries;
        set => checkBoundaries = value;
    }

    public void SetLimits(float l, float r)
    {
        minX = l;
        maxX = r;
    }

    private void Update()
    {
        playerMovement.MoveCamera = ShouldMove();
    }

    private bool ShouldMove()
    {
        var position = transform.position;
        var x = player.position.x;

        var currentlyLeft = _prevX > x;
        var currentlyRight = _prevX < x;

        if(checkBoundaries)
            if (position.x < minX && currentlyLeft || 
                position.x > maxX && currentlyRight) return false;

        var min = position.x - diff;
        var max = position.x + diff;




        if (_left && currentlyRight)
        {
            _left = x < min;
            return false;
        }
        
        if (_right && currentlyLeft)
        {
            _right = x > max;
            return false;
        }
        
        _left = x < min;
        _right = x > max;
        

        _prevX = x;


        return _left || _right;

    }

    private bool _left, _right;
    private float _prevX;
}
