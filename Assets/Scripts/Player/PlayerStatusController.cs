using System;
using UnityEngine;

public class PlayerStatusController : MonoBehaviour
{
    
    #region Singleton

    private static PlayerStatusController _instance;
    public static PlayerStatusController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<PlayerStatusController>();

            return _instance;
        }
    } 
    
    #endregion


    [SerializeField] private PlayerInteraction interaction;
    [SerializeField] private PlayerMovement movement;
    [SerializeField] private Animator myAnimator;

    private IStatus _status;

    private NormalStatus _normal;
    private InactiveStatus _inactive;

    public void SetAnimatorSpeed(float f) => myAnimator.speed = f;

    public void SetNormal() => _status = _normal;

    public void SetInactive()
    {
        _status = _inactive;
        myAnimator.SetBool("Walking", false);
    }

    private void Awake()
    {
        _instance = this;
        
        _normal = new NormalStatus(movement, interaction, this);
        _inactive = new InactiveStatus(movement, interaction, this);

        _status = _normal;
    }

    private void Update()
    {
        if (!Input.GetButtonDown("Vertical")) return;

        _y = Input.GetAxis("Vertical");


        if ((!(_y > 0) || !_status.CanInteractUp()) && (!(_y < 0) || !_status.CanInteractLow())) return;
        
        SetInactive();
        myAnimator.SetTrigger("Interact");

    }

    private float _y;

    public void ANIM_Interact()
    {
        SetNormal();
        myAnimator.speed = 0;
        
        if(_y > 0)
            _status.InteractUp();
        else if (_y < 0)
            _status.InteractDown();
    }

    private void FixedUpdate()
    {
        var x = Input.GetAxis("Horizontal");

        if (x != 0)
            _status.Move(x);
        else
            myAnimator.SetBool("Walking", false);
    }

}
