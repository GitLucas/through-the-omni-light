public interface IStatus
{
    public void Move(float x);
    public void InteractUp();
    public void InteractDown();
    public bool CanInteractUp();
    public bool CanInteractLow();
}
