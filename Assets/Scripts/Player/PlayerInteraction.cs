using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    [SerializeField] private InteractableDetector upper, lower;

    public bool CanInteractUp() => upper.Interactable != null;
    public bool CanInteractLow() => lower.Interactable != null;
    
    public void InteractUp(Action callback = null)
    {
        if (!upper.Interactable) return;
            
        upper.Interactable.Interact();
        callback?.Invoke();
    }
    
    public void InteractDown(Action callback = null)
    {
        if (!lower.Interactable) return;
        
        lower.Interactable.Interact();
        callback?.Invoke();
    }
}
