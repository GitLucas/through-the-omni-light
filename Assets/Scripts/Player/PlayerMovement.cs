using System.Runtime.CompilerServices;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D myRigidbody;
    [SerializeField] private SpriteRenderer mySpriteRenderer;
    [SerializeField] private Animator myAnimator;
    private Transform _camTransform;

    public bool MoveCamera
    {
        get; 
        set;
    }

    private void Start()
    {
        if (Camera.main != null) _camTransform = Camera.main.transform;
    }

    public void ANIM_Footstep()
    {
        SfxController.Instance.PlayFootstep();
    }
    
    public void Move(float x)
    {
        var d = Mathf.Sign(x);
        var movem = Vector3.right * (d * Time.fixedDeltaTime * 10);

        mySpriteRenderer.flipX = d < 0;
        myAnimator.SetBool("Walking", true);
        
        if (MoveCamera)
            _camTransform.position += movem;
        
        myRigidbody.MovePosition(transform.position + movem);
        
    }
}
