using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalStatus : IStatus
{

    private PlayerMovement _movement;
    private PlayerInteraction _interaction;
    private PlayerStatusController _controller;
    
    public NormalStatus(PlayerMovement m, PlayerInteraction i, PlayerStatusController psc)
    {
        _movement = m;
        _interaction = i;
        _controller = psc;
    }
    
    
    public void Move(float x)
    {
        _movement.Move(x);
    }

    public void InteractUp()
    {
        _interaction.InteractUp( () => {_controller.SetInactive();});
    }

    public void InteractDown()
    {
        _interaction.InteractDown( () => {_controller.SetInactive();});
    }

    public bool CanInteractUp() => _interaction.CanInteractUp();
    public bool CanInteractLow() => _interaction.CanInteractLow();
}
