using System;
using UnityEngine;

public class InteractableDetector : MonoBehaviour
{
    private Interactable _interactable;

    public Interactable Interactable => _interactable;

    private void Start()
    {
        _interactable = null;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        _interactable = other.GetComponent<Interactable>();
        _interactable?.HighlightObject(true);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _interactable?.HighlightObject(false);
        _interactable = null;
    }
}
