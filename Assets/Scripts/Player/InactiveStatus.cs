using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InactiveStatus : IStatus
{
    public InactiveStatus(PlayerMovement m, PlayerInteraction i, PlayerStatusController psc) { }
    public void Move(float x) { }
    public void InteractUp() { }
    public void InteractDown() { }
    public bool CanInteractUp() => false;
    public bool CanInteractLow() => false;
}
